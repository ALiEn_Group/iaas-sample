#!/usr/bin/env bash
set -e
TF_IN_AUTOMATION="true"

cd tf

echo "tf-init-----------------------------------------------"

terraform init -reconfigure \
               -get=true \
               -get-plugins=true \
               -backend-config="token=$TF_REMOTE_TOKEN" \
               -backend-config="$CI_PROJECT_DIR/envs/$TARGET_ENVIRONMENT/backend.hcl"
