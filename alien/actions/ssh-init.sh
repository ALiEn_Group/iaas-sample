#!/usr/bin/env bash

echo "ssh-init-----------------------------------------------"

mkdir -p ~/.ssh
chmod 700 ~/.ssh
cp $CI_PROJECT_DIR/config/id_rsa ~/.ssh/ghost_id_rsa
chmod 600 ~/.ssh/ghost_id_rsa
cp $CI_PROJECT_DIR/config/inventory/ssh_config ~/.ssh/config
chmod 400 ~/.ssh/config
