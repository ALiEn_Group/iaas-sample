#!/usr/bin/env bash
set -e

cd tf

echo "tf-output-----------------------------------------------"


terraform output -json > output.json
terraform output ansible_hosts > $CI_PROJECT_DIR/config/inventory/hosts
terraform output ssh_config > $CI_PROJECT_DIR/config/inventory/ssh_config

