#!/usr/bin/env bash
set -e

# ALiEn root path
LOCAL_DIR="$ALIEN_MODULE_PATH/../"

# Custom scripts in VNF repo folder
REMOTE_DIR="$CI_PROJECT_DIR/alien/"

# Flow of the actions
# Example:
#"$LOCAL_DIR"actions/script1.sh
#"$LOCAL_DIR"actions/script2.py
#"$REMOTE_DIR"actions/custom_script.sh

echo "-------------------------------------------------------------------"
echo "                      Starting destroy flow"
echo "-------------------------------------------------------------------"
"$REMOTE_DIR"actions/tf-init.sh
"$LOCAL_DIR"actions/tf-destroy-plan.sh
"$LOCAL_DIR"actions/tf-destroy-apply.sh
