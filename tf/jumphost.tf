# Resources for creating servers, ports, routes 
resource "openstack_compute_instance_v2" "jumphost" {
  name              = "${var.openstack_tenant_name}-jh"
  image_name        = data.openstack_images_image_v2.jh-image.name
  availability_zone = var.jumphost_az
  flavor_id         = data.openstack_compute_flavor_v2.jumphost.id
  key_pair          = "${var.openstack_tenant_name}-admin_key"
  network {
    name = openstack_networking_network_v2.ghost_oam.name
    port = openstack_networking_port_v2.jumphost_oam_port.id
  }
}

resource "openstack_networking_port_v2" "jumphost_oam_port" {
  name       = "jumphost_internal_port"
  network_id = openstack_networking_network_v2.ghost_oam.id
  security_group_ids = [
    data.openstack_networking_secgroup_v2.sec-default.id,
    openstack_networking_secgroup_v2.icmp_secgroup.id,
    openstack_networking_secgroup_v2.ssh_secgroup.id,
  ]
  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.ghost_oam_ipv4.id
    ip_address = var.jumphost_oam_ip
  }
  admin_state_up = "true"
}

# Floating IP
resource "openstack_networking_floatingip_v2" "floatip_jumphost" {
  pool = "iaas_shared_internet-0"
}

resource "openstack_networking_floatingip_associate_v2" "fip_jumphost" {
  floating_ip = openstack_networking_floatingip_v2.floatip_jumphost.address
  port_id     = openstack_networking_port_v2.jumphost_oam_port.id
}

