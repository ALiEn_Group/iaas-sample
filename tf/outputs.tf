# Output blocks are used for exporting or returning values by a Terraform module. And these outputs are exposed or printed for next usage.

data "template_file" "web_cluster_hosts" {
  template = file("templates/cluster_hosts.tpl")
  count    = var.ghost-web_count
  vars = {
    node    = openstack_compute_instance_v2.ghost-web[count.index].name
    node_ip = openstack_compute_instance_v2.ghost-web[count.index].access_ip_v4
  }
}

data "template_file" "db_cluster_hosts" {
  template = file("templates/cluster_hosts.tpl")
  count    = var.ghost-db_count
  vars = {
    node    = openstack_compute_instance_v2.ghost-db[count.index].name
    node_ip = openstack_compute_instance_v2.ghost-db[count.index].access_ip_v4
  }
}

data "template_file" "ansible_hosts" {
  template = file("templates/ansible_hosts.tpl")
  vars = {
    web         = join("\n", data.template_file.web_cluster_hosts.*.rendered)
    db          = join("\n", data.template_file.db_cluster_hosts.*.rendered)
    jumphost_ip = openstack_networking_floatingip_v2.floatip_jumphost.address
  }
}

data "template_file" "web_cluster_ssh" {
  template = file("templates/cluster_ssh.tpl")
  count    = var.ghost-web_count
  vars = {
    node          = openstack_compute_instance_v2.ghost-web[count.index].name
    node_ip       = openstack_compute_instance_v2.ghost-web[count.index].access_ip_v4
    jumphost_name = openstack_compute_instance_v2.jumphost.name
  }
}

data "template_file" "db_cluster_ssh" {
  template = file("templates/cluster_ssh.tpl")
  count    = var.ghost-db_count
  vars = {
    node          = openstack_compute_instance_v2.ghost-db[count.index].name
    node_ip       = openstack_compute_instance_v2.ghost-db[count.index].access_ip_v4
    jumphost_name = openstack_compute_instance_v2.jumphost.name
  }
}

data "template_file" "ssh_config" {
  template = file("templates/ssh_config.tpl")
  vars = {
    web           = join("\n", data.template_file.web_cluster_ssh.*.rendered)
    db            = join("\n", data.template_file.db_cluster_ssh.*.rendered)
    jumphost_ip   = openstack_networking_floatingip_v2.floatip_jumphost.address
    jumphost_name = openstack_compute_instance_v2.jumphost.name
  }
}

output "ansible_hosts" {
  value = "${data.template_file.ansible_hosts.rendered}\n\n\n"
}

output "ssh_config" {
  value = data.template_file.ssh_config.rendered
}

output "ghost_web_ip" {
  value = "\n${openstack_networking_floatingip_v2.floatip_web.address}\n\n\n"
}
