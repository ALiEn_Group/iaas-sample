# Resources for creating servers, ports, routes 
resource "openstack_compute_instance_v2" "ghost-db" {
  count             = var.ghost-db_count
  name              = format("ghost-db%02d", count.index + 1)
  image_name        = data.openstack_images_image_v2.db-image.name
  availability_zone = element(var.availability_zones, count.index)
  flavor_id         = data.openstack_compute_flavor_v2.ghost-db.id
  key_pair          = "${var.openstack_tenant_name}-admin_key"
  network {
    name           = openstack_networking_network_v2.ghost_oam.name
    port           = openstack_networking_port_v2.ghost-db_oam_port[count.index].id
    access_network = true
  }
  network {
    name = openstack_networking_network_v2.ghost_m2m1.name
    port = openstack_networking_port_v2.ghost-db_m2m1_port[count.index].id
  }
}

### OAM port
resource "openstack_networking_port_v2" "ghost-db_oam_port" {
  count      = var.ghost-db_count
  name       = format("ghost-db_oam_port%02d", count.index + 1)
  network_id = openstack_networking_network_v2.ghost_oam.id
  security_group_ids = [
    data.openstack_networking_secgroup_v2.sec-default.id,
    openstack_networking_secgroup_v2.ssh_secgroup.id,
    openstack_networking_secgroup_v2.icmp_secgroup.id,
  ]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.ghost_oam_ipv4.id
  }
  admin_state_up = "true"
}

### M2M1 port
resource "openstack_networking_port_v2" "ghost-db_m2m1_port" {
  count      = var.ghost-db_count
  name       = format("ghost-db_m2m1_port%02d", count.index + 1)
  network_id = openstack_networking_network_v2.ghost_m2m1.id
  security_group_ids = [
    openstack_networking_secgroup_v2.ghost-db_m2m1_secgroup.id,
    openstack_networking_secgroup_v2.ghost-swarm_m2m1_secgroup.id,
    openstack_networking_secgroup_v2.icmp_secgroup.id,
  ]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.ghost_m2m1_ipv4.id
  }
  allowed_address_pairs {
    ip_address = var.ghost-db_vrrp_ip
  }
  admin_state_up = "true"
}

# Security groups and rules
resource "openstack_networking_secgroup_v2" "ghost-db_m2m1_secgroup" {
  name        = "ghost-db_m2m1_secgroup"
  description = "secgroup for ghost-db on m2m1 network"
}

resource "openstack_networking_secgroup_rule_v2" "ghost-db_m2m1_db" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ghost-db_m2m1_secgroup.id
}

