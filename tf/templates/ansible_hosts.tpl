
jumphost ansible_host=${jumphost_ip}

[vnf-ghost:children]
web
db

[web]
${web}

[db]
${db}

[vnf-ghost:vars]
ansible_ssh_common_args=' -o ProxyCommand="ssh -W %h:%p -q jumphost"'
