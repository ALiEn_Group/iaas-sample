# Data sources are used for getting the IDs of an available OpenStack resources (prerequisites).

data "openstack_compute_flavor_v2" "jumphost" {
  name = "sn1.tiny"
}

data "openstack_compute_flavor_v2" "ghost-web" {
  name = "sn1.small"
}

data "openstack_compute_flavor_v2" "ghost-db" {
  name = "sn2.small"
}

data "openstack_images_image_v2" "web-image" {
  name = var.ghost-web_image_name
}

data "openstack_images_image_v2" "db-image" {
  name = var.ghost-db_image_name
}

data "openstack_images_image_v2" "jh-image" {
  name = var.jumphost_image_name
}

data "openstack_networking_router_v2" "router" {
  status = "ACTIVE"
}

data "openstack_networking_secgroup_v2" "sec-default" {
  name = "default"
}

