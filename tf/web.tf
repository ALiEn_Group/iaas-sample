# Resources for creating servers, ports, routes 
resource "openstack_compute_instance_v2" "ghost-web" {
  count             = var.ghost-web_count
  name              = format("ghost-web%02d", count.index + 1)
  image_name        = data.openstack_images_image_v2.web-image.name
  availability_zone = element(var.availability_zones, count.index)
  flavor_id         = data.openstack_compute_flavor_v2.ghost-web.id
  key_pair          = "${var.openstack_tenant_name}-admin_key"
  network {
    name           = openstack_networking_network_v2.ghost_oam.name
    port           = openstack_networking_port_v2.ghost-web_oam_port[count.index].id
    access_network = true
  }
  network {
    name = openstack_networking_network_v2.ghost_m2m1.name
    port = openstack_networking_port_v2.ghost-web_m2m1_port[count.index].id
  }
}

### OAM port
resource "openstack_networking_port_v2" "ghost-web_oam_port" {
  count      = var.ghost-web_count
  name       = format("ghost-web_oam_port%02d", count.index + 1)
  network_id = openstack_networking_network_v2.ghost_oam.id
  security_group_ids = [
    data.openstack_networking_secgroup_v2.sec-default.id,
    openstack_networking_secgroup_v2.ssh_secgroup.id,
    openstack_networking_secgroup_v2.icmp_secgroup.id,
    openstack_networking_secgroup_v2.ghost-web_oam_secgroup.id,
  ]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.ghost_oam_ipv4.id
  }
  admin_state_up = "true"
}

# Security groups and rules
resource "openstack_networking_secgroup_v2" "ghost-web_oam_secgroup" {
  name        = "ghost-web_oam_secgroup"
  description = "secgroup for ghost-web on oam network"
}

resource "openstack_networking_secgroup_rule_v2" "ghost-web_oam_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ghost-web_oam_secgroup.id
}

# Floating IP
resource "openstack_networking_floatingip_v2" "floatip_web" {
  pool = "iaas_shared_internet-0"
}

resource "openstack_networking_floatingip_associate_v2" "fip_web" {
  floating_ip = openstack_networking_floatingip_v2.floatip_web.address
  port_id     = openstack_networking_port_v2.ghost-web_oam_port[0].id
}

### M2M1 port
resource "openstack_networking_port_v2" "ghost-web_m2m1_port" {
  count      = var.ghost-web_count
  name       = format("ghost-web_m2m1_port%02d", count.index + 1)
  network_id = openstack_networking_network_v2.ghost_m2m1.id
  security_group_ids = [
    openstack_networking_secgroup_v2.ghost-swarm_m2m1_secgroup.id,
    openstack_networking_secgroup_v2.icmp_secgroup.id,
  ]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.ghost_m2m1_ipv4.id
  }
  admin_state_up = "true"
}

