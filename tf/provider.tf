# Provider is responsible for understanding API interactions and exposing resources.
# Provider has to be specified that Terraform knows what infrastructure type can be represented as a resource in Terraform.

provider "openstack" {
  version = "1.20.0"
}

provider "template" {
  version = "2.1.2"
}

