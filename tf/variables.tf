# Input variables serve as parameters for a Terraform module, allowing aspects of the module to be customized without altering the module's own source code, and allowing modules to be shared between different configurations.
# Input variables with empty values are defined under environment tfvars variables.

variable "openstack_tenant_name" {
}

variable "openstack_user_name" {
}

variable "tenant_admin_public_key" {
}

variable "ghost_oam_cidr" {
  default = "192.168.0.0/24"
}

variable "ghost_oam_allocation_start" {
  default = "192.168.0.10"
}

variable "ghost_oam_allocation_end" {
  default = "192.168.0.100"
}

variable "ghost_oam_dns_nameserver" {
  default = "8.8.8.8"
}

variable "ghost_m2m1_cidr" {
  default = "192.168.10.0/24"
}

variable "ghost_m2m1_allocation_start" {
  default = "192.168.10.10"
}

variable "ghost_m2m1_allocation_end" {
  default = "192.168.10.100"
}

variable "jumphost_count" {
  default = 1
}

variable "jumphost_image_name" {
  default = "ubuntu-16.04-x86_64"
}

variable "jumphost_az" {
  default = "az1"
}

variable "jumphost_oam_ip" {
  default = "192.168.0.5"
}

variable "ghost-web_count" {
  default = 1
}

variable "ghost-web_image_name" {
  default = "ubuntu-16.04-x86_64"
}

variable "ghost-db_count" {
  default = 3
}

variable "ghost-db_image_name" {
  default = "ubuntu-16.04-x86_64"
}

variable "ghost-db_vrrp_ip" {
  default = "192.168.10.4"
}

# AZ variables for Beryllium cloud version
variable "availability_zones" {
  type    = list(string)
  default = ["az1", "az2", "az3"]
}

