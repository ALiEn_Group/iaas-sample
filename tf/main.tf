### General
# Security groups and rules
resource "openstack_networking_secgroup_v2" "icmp_secgroup" {
  name        = "icmp_secgroup"
  description = "secgroup allowing icmp"
}

resource "openstack_networking_secgroup_rule_v2" "icmp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.icmp_secgroup.id
}

# Network setup
### OAM
resource "openstack_networking_network_v2" "ghost_oam" {
  name           = "${var.openstack_tenant_name}-oam"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "ghost_oam_ipv4" {
  name       = "${var.openstack_tenant_name}-oam_ipv4"
  network_id = openstack_networking_network_v2.ghost_oam.id
  cidr       = var.ghost_oam_cidr
  ip_version = 4
  allocation_pools {
    start = var.ghost_oam_allocation_start
    end   = var.ghost_oam_allocation_end
  }
  dns_nameservers = [var.ghost_oam_dns_nameserver]
}

### OAM Security groups and rules
resource "openstack_networking_secgroup_v2" "ssh_secgroup" {
  name        = "ssh_secgroup"
  description = "secgroup allowing ssh access"
}

resource "openstack_networking_secgroup_rule_v2" "ssh_tcp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ssh_secgroup.id
  depends_on        = [openstack_networking_secgroup_v2.ssh_secgroup]
}

resource "openstack_networking_secgroup_rule_v2" "ssh_udp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ssh_secgroup.id
  depends_on        = [openstack_networking_secgroup_rule_v2.ssh_tcp]
}

### M2M1
resource "openstack_networking_network_v2" "ghost_m2m1" {
  name           = "${var.openstack_tenant_name}-m2m1"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "ghost_m2m1_ipv4" {
  name       = "${var.openstack_tenant_name}-m2m1_ipv4"
  network_id = openstack_networking_network_v2.ghost_m2m1.id
  cidr       = var.ghost_m2m1_cidr
  ip_version = 4
  allocation_pools {
    start = var.ghost_m2m1_allocation_start
    end   = var.ghost_m2m1_allocation_end
  }
}

### M2M1 Security groups and rules
resource "openstack_networking_secgroup_v2" "ghost-swarm_m2m1_secgroup" {
  name        = "ghost-swarm_m2m1_secgroup"
  description = "secgroup for ghost swarm on m2m1 network"
}

resource "openstack_networking_secgroup_rule_v2" "docker-swarm" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 2377
  port_range_max    = 2377
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ghost-swarm_m2m1_secgroup.id
  depends_on        = [openstack_networking_secgroup_v2.ghost-swarm_m2m1_secgroup]
}

resource "openstack_networking_secgroup_rule_v2" "swarm-nodes-tcp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 7946
  port_range_max    = 7946
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ghost-swarm_m2m1_secgroup.id
  depends_on        = [openstack_networking_secgroup_rule_v2.docker-swarm]
}

resource "openstack_networking_secgroup_rule_v2" "swarm-nodes-udp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 7946
  port_range_max    = 7946
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ghost-swarm_m2m1_secgroup.id
  depends_on        = [openstack_networking_secgroup_rule_v2.swarm-nodes-tcp]
}

resource "openstack_networking_secgroup_rule_v2" "swarm-vxlan" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 4789
  port_range_max    = 4789
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ghost-swarm_m2m1_secgroup.id
  depends_on        = [openstack_networking_secgroup_rule_v2.swarm-nodes-udp]
}

# Logical router
resource "openstack_networking_router_interface_v2" "router_interface" {
  router_id = data.openstack_networking_router_v2.router.id
  subnet_id = openstack_networking_subnet_v2.ghost_oam_ipv4.id
}

# Key pair
resource "openstack_compute_keypair_v2" "admin_key" {
  name       = "${var.openstack_tenant_name}-admin_key"
  public_key = var.tenant_admin_public_key
}

