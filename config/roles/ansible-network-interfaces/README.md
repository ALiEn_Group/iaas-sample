# Configure Network interfaces

This role is able to manage Linux network interfaces includes
- static and dhcp configuration
- ipv4 and ipv6 supported
- network and host routes, next hop and source addresses
- multiple IP addresses on an interface include source address set.

The ansible role will detect (via facts) available ethernet interfaces. Default
behavior is to bring up all available interfaces via dhcp (ipv4) and set default
route on the first interface.

Nevertheless network interfaces can be managed one-by-one by this playbook.

This role is able to manage the lifecycle of interfaces as well, means you can
modify (eg: routes) and remove interfaces according to definition in
`ether_interfaces` variable.

There is a possibility to make this role to run only for initial server configuration. Re-runs will skip the config part.
This can be useful if the role is being used in the pipeline where additional network configuration
is done in later stage of the pipeline. For more on this, check `only_initial_config_flag` variable.

## Requirements

- OS: Ubuntu

- OS: CentOS/RHEL  
  Compatible with `Defaults` (all interfaces up, dhcp, default GW on the first interface)  
  Partial compatibility with `ether_interfaces` (default GW can be on other than first interface)

## Role Variables

- `only_initial_config_flag`: "false" (role default)  
  If this role variable is "true" the network configuration will be done only once per host.
  After successful configuration, the role will create a flag file on the controlled host (/etc/ansible/only_initial_config).
  This will prevent the configuration to be done twice on the same host by re-running the playbook.  

- `ether_interfaces` - input variable stores the interface configuration.

Naming of network interfaces can be different by linux distributions and versions.
(https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/)

In cloud environment the sequence order of interfaces is well defined in higher
level descriptors like TOSCA. So the sequence of `dict` in `ether_interfaces`
variable should be aligned with the sequence of interfaces attached to a VM
described by VNF descriptors.

## Dependencies

- ansible version: 2.X (tested on 2.5.3 and 2.7.4)
- jinja2 version: 2.10

## Example Playbooks

Detailed test cases can be found in `test` directory

### Defaults (without defined `ether_interfaces` variable)

No `ether_interfaces` variables defined. All interfaces will be up and running,
method is `dhcp` (ipv4). Default route will be on the first interface.

```
---

- hosts:        all
  become:       yes
  gather_facts: true
  roles:
    - role: ansible-network-interfaces

```

### Interface configuration using `ether_interfaces` variable

Default GW on the second interface.

```
---

- hosts:        all
  become:       yes
  gather_facts: true
  roles:
    - role: ansible-network-interfaces
      ether_interfaces:
        - descrition: oam
          ipv4:
            method: dhcp
        - descrition: m2m1
          ipv4:
            method: dhcp
            default: True            
```

### ipv4 and ipv6 dual stack interface configuration

Dual stack (dhcp) configured on the second interface.

```
---

- hosts:        all
  become:       yes
  gather_facts: true
  roles:
    - role: ansible-netwoDefault GWrk-interfaces
      ether_interfaces:
        - descrition: oam
          ipv4:
            method: dhcp
            default: True
        - descrition: m2m6
          ipv6:
            method: dhcp
          ipv4:
            method: dhcp

```

### Linux route test cases

Includes static routes life cycle: create, modify and delete operations.
Nexthop address can be configured for static routes.

```
---

- hosts:        all
  become:       yes
  gather_facts: true
  roles:
    - role: ansible-network-interfaces
      ether_interfaces:
        - descrition: oam
          ipv4:
            method: dhcp
            routes:
              - network: 192.168.123.0/24
              - network: 192.168.124.0/24
        - descrition: m2m1
          ipv4:
            method: dhcp
            default: True
            method: dhcp
            routes:
              - network: 192.168.199.0/24
                nexthop: 192.168.1.253
              - host: 192.168.200.111
                nexthop: 192.168.1.253            
        - descrition: m2m6
          ipv6:
            method: dhcp
            routes:
              - network: "fdfd:a:0:0::/64"
```




### Secondary address test case I.

Includes life cycle: create, modify and delete operations.

Two secondary addresses are configured on `m2m1` interface. Source IP can be
defined for routes, for example there is a static route 192.168.123.0/24
where source IP 192.168.1.6 is assigned.

```
---

- hosts:        all
  become:       yes
  gather_facts: true
  roles:
    - role: ansible-network-interfaces
      ether_interfaces:
        - descrition: oam
          ipv4:
            method: dhcp
        - descrition: m2m1
          ipv4:
            method: dhcp
            default: True
            addresses:
              - 192.168.1.5
              - 192.168.1.6
            routes:
              - network: 192.168.123.0/24
                source: 192.168.1.6
        - descrition: m2m6
          ipv6:
            method: dhcp
            addresses:
              - fd00:1:1:1::10
            routes:
              - network: "fdfd:a:0:0::/64"
              - network: "fdfd:a:0:1::/64"
```

### Secondary address test case II.

Secondary addresses are configured on `m2m1` interface. Source IP can be
defined for `default` route, for example source IP 192.168.1.6 is assigned to
default route below.

```
---

- hosts:        all
  become:       yes
  gather_facts: true
  roles:
    - role: ansible-network-interfaces
      ether_interfaces:
        - descrition: oam
          ipv4:
            method: dhcp
        - descrition: m2m1
          ipv4:
            method: dhcp
            default: True
            addresses:
              - 192.168.1.5
              - 192.168.1.6
            routes:
              - network: default            
                source: 192.168.1.6
                nexthop: 192.168.1.1
```

**Notes**

If `default` route is defined in interface (`default: True`) level and `default`
route is defined in `routes`, definition in routes has priority and will be
applied only.

In case of IPv6 default route definition in routes level will not be considered,
just in interface level.

## License

BSD

## Author Information

Pan-Net

## TODO

- Support interface deactivation in the middle of `ether_interface` definition
- CentOS support (WIP)
