
# Test cases

## Create VMs

Create test VMs in `sandbox` tenant. Three network interface port are attached to VMs, `oam`, `m2m1` and `m2m2`.

```
source <os.sh>
nova boot --flavor=m1.small --image="ubuntu-14.04.5-x86_64" --nic net-name=sandbox_oam --nic net-name=sandbox_m2m1 --nic net-name=sandbox_m2m6 --availability-zone=MGMT --key-name sandbox-phase3_operator_keypairs ansible-test2
nova boot --flavor=m1.small --image="ubuntu-16.04.1-x86_64" --nic net-name=sandbox_oam --nic net-name=sandbox_m2m1 --nic net-name=sandbox_m2m6 --availability-zone=MGMT --key-name sandbox-phase3_operator_keypairs ansible-test1
```

## Create ansible host file

According to README in `dynamic_inventory` directory.

## Execute test cases

The following test cases should be executed one-by-one.

### Configuration without any additional definition

No `ether_interfaces` variables defined. All interfaces will be up and running, method is `dhcp`.
Default route will be on the first interface.

```
ansible-playbook ansible-if-test.0.1.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
```

### Configuration of the first and second interfaces

`ether_interfaces` variables is defined. Two interfaces will be up and running, method is `dhcp`.
Default route will be on the second interface.

```
ansible-playbook ansible-if-test.1.1.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
```                  	

### Configuration of all interfaces (`ether_interfaces`)

`ether_interfaces` variables is defined. All of three interfaces will be up and running, method is `dhcp`.
Default route will be moved to the first interface.

```
ansible-playbook ansible-if-test.1.2.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
```

### Configuration of all interfaces - dual stack (`ether_interfaces`)

`ether_interfaces` variables is defined. All of three interfaces will be up and running, method is `dhcp`.
Dual stack (ipv4/ipv6) is defined on third interface.

```
ansible-playbook ansible-if-test.1.3.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
```

### Configuration of all interfaces - dual stack (`ether_interfaces`)

`ether_interfaces` variables is defined. All of three interfaces will be up and running, method is `dhcp`.
Default route will be moved to the second interface. Dual stack (ipv4/ipv6) is defined on third interface.

```
ansible-playbook ansible-if-test.1.4.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
```

### Configuration of all interfaces - dual stack (`ether_interfaces`)

Negative test: `ether_interfaces` defines more interfaces than VMs have.

```
ansible-playbook ansible-if-test.1.5.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
```

### Linux route test cases

Includes static routes life cycle: create, modify and delete operations.

```
ansible-playbook ansible-route-test.1.0.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
ansible-playbook ansible-route-test.1.1.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
ansible-playbook ansible-route-test.1.2.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
```

### Secondary address test cases

Includes life cycle: create, modify and delete operations.

```
ansible-playbook ansible-addr-test.1.0.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
ansible-playbook ansible-addr-test.1.1.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
ansible-playbook ansible-addr-test.1.3.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
ansible-playbook ansible-addr-test.1.4.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
```

### Static interface test cases

> Experimental

```
ansible-playbook ansible-static-test0.yaml -e 'ansible_python_interpreter=/usr/bin/python3'
```

# `ether_interfaces` Parameters

```
---

- hosts:        all
  become:       yes
  gather_facts: true
  roles:
    - role: ansible-network-interfaces
      ether_interfaces:
        - descrition: oam
          ipv4:
            method: static
            addresses:
              - 1.1.1.11
              - 1.1.1.12
              - 1.1.1.13
            cidr: 1.1.1.11/24
            gateway: 192.168.4.253
            routes:
              - network: 192.168.123.0/24
                nexthop: 192.168.4.254
                source: 1.1.1.12
              - host: 8.8.8.8
                nexthop: 192.168.4.253
                source: 1.1.1.13               
          ipv6:
            method: static
            addresses:
              - "fdfd:0:0::101"
              - "fdfd:0:0::102"
              - "fdfd:0:0::103"
            cidr: "fdfd:0:0::100/64"
            gateway: "fdfd:0:0::1/64"
            routes:
              - network: "fdfd:0:1::/64"
                nexthop: "fdfd:0:0::aaa/64"
                source: "fdfd:0:0::101/64"
              - host: "fdfd:0:2::fff"
                nexthop: "fdfd:0:0::bbb/64"
                source: "fdfd:0:0::102/64"

        - descrition: m2m1
          ipv4:
            method: dhcp
            default: True
            addresses:
              - 1.1.1.11/32
              - 1.1.1.12/32
              - 1.1.1.13/32
            routes:
              - network: default
                source: 1.1.1.11
              - network: 192.168.123.0/24
                nexthop: 192.168.4.254
                source: 1.1.1.12
              - host: 8.8.8.8
                nexthop: 192.168.4.253
                source: 1.1.1.13               

```
