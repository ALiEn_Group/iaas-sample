#!/usr/bin/env python

#
# This script creates a hosts file for ansible out of an <inventoryfile>
# created using a dynamic inventory
# The hostfile will have this format:
#
# <server name> ansible_host=<ip address of _oam interface> [ansible_user=<ansible username>] [ansible_ssh_common_args='-o ProxyCommand="ssh -W %h:%p -q  ansible_user=<ansible username>@<jumphost ip address>"]
#

import argparse
from collections import defaultdict
import json
import sys
import re

def create_hostfile(inventoryfile):
    with open(inventoryfile, 'r') as json_data:
        data = json.load(json_data)
        server_dict = defaultdict(list)
        host_entry = ''
        server = ''
        counted_instances = 0

        for instance in data['_meta']['hostvars']:
            server = data['_meta']['hostvars'][instance]['openstack']['name']
            for ip_srv_name in data['_meta']['hostvars'][instance]['openstack']['addresses']:
                if ip_srv_name.endswith('_oam'):
                    ip_srv_ip = data['_meta']['hostvars'][instance]['openstack']['addresses'][ip_srv_name][0]['addr']
                    host_entry = "\t"+"ansible_host="+ip_srv_ip
            if host_entry not in server_dict[server]:
                server_dict[server].append(host_entry)
                counted_instances +=1

    print("counted_instances: {}".format(counted_instances))
    return server_dict

def write_hostfile(hostfile, extracted, ansible_user=None, ansible_jumphost_ip=None, ansible_jumphost_name=None):
    hostfile = hostfile
    user = ''
    proxycommand = ''
    inventoryFile = open(hostfile, 'w')
    for key, value in sorted(extracted.items()):
        inventoryFile.write(key)
        for item in sorted(value):
            if ansible_user is not None:
                user = " ansible_user="+ansible_user
                userpart = ansible_user+"@"
            else:
                user = ""
                userpart = ""
            if ansible_jumphost_ip is not None and ansible_jumphost_name is not None:
                if ansible_jumphost_name in key:
                    item = "\t"+"ansible_host="+ansible_jumphost_ip
                else:
                    proxycommand = " ansible_ssh_common_args='-o ProxyCommand=\"ssh -W %h:%p -q "+userpart+ansible_jumphost_ip+"\"'"
            inventoryFile.write(item + user + proxycommand)
        inventoryFile.write("\n")
    inventoryFile.close()

def main():
    parser = argparse.ArgumentParser(description='Generate a hostfile out of an inventory.')
    parser.add_argument('--inventory', default='inventory.json', help='the inventory file in json format to read from')
    parser.add_argument('--hostfile', default='ansible_hosts', help='the host file in config format to write to')
    parser.add_argument('--user',  help='ansible user name')
    parser.add_argument('--jumphost_ip', help='jumphost address for proxycommand')
    parser.add_argument('--jumphost_name', help='jumphost name')
    args = parser.parse_args()

    inventoryFile = args.inventory
    hostfile = args.hostfile
    ansible_user = args.user
    ansible_jumphost_ip = args.jumphost_ip
    ansible_jumphost_name = args.jumphost_name

    ansible_host_file = create_hostfile(inventoryFile)
    write_hostfile(hostfile, ansible_host_file, ansible_user, ansible_jumphost_ip, ansible_jumphost_name)

    with open(hostfile, 'r') as finalfile:
        print finalfile.read()

if __name__ == "__main__":
    sys.exit(main())
