
# Generate inventory file

## Steps
1. Source your OpenStack RC file and enter your password
```source <your-name>-openrc.sh```
2. Create the file containing all inventory related information
```python openstack.py --list > inventory.json```
3. Create ansible_host out of inventory.json group using roles:
```python host_inventory.py --jumphost_ip=<JH_IP> --jumphost_name=<JH_NAME> --user=ubuntu```

# Create `host_vars`

## Steps
1. Generate from TOSCA
```generator -i ~/pannet/vnf/VNF_itot/vnfd/<TOSCA_NAME>.yaml -t templates/ansible_host_vars.j2 -o generated```
2. rename directory
```mv generated host_vars```
3. edit var file (add VIP addresses)

# Execute playbook - configure interfaces

## Steps
1. install requirements
```ansible-galaxy install -r requirements.yml```
2. Configure jumphost
```ansible-playbook --extra-vars "variable_host=<JH_NAME>" interfaces.yaml```
3. Configure all other hosts
```ansible-playbook interfaces.yaml```
