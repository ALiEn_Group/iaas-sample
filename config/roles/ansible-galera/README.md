ansible-docker
==============

This role deploy [mariadb galera](https://github.com/colinmollenhour/mariadb-galera-swarm) in swarm


Requirements
------------

Should work on vanilla Ubuntu server.

Role Variables
--------------

Apart from image variables there are:
* `LABEL`: Swarm label on node to be used for image placement
* `SEED_REPLICAS`: Number of galera masters to deploy ( 0 - 1)
* `NODE_REPLICAS`: Number of galera ndoes to deploy 
* `GALERA_CHECK_HOSTS`: Hosts to check whether replicas running in cluster on not

Authors
------------------

Alien Team
