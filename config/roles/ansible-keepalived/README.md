ansible-docker
==============

This role deploy [keepaliveded docker image](https://github.com/osixia/docker-keepalived)


Requirements
------------

Should work on vanilla Ubuntu server.

Role Variables
--------------

* `interface`: Network interface to bind floating IP
* `virtual_ipaddresses`: IP floating between hosts

Authors
------------------

Alien Team
