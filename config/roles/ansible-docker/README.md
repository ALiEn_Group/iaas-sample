ansible-docker
==============

This role installs Docker CE with a http proxy for reaching images


Requirements
------------

Should work on vanilla Ubuntu server.

Role Variables
--------------

* `docker_proxy_enabled`: defaults to true - then you need to provide http/https proxy addresses below
* `http_proxy_address`:   HTTP proxy in the form of URL
* `https_proxy_address`:  HTTPS proxy in the form of URL (note proxy for HTTPS may actually be exposed via HTTP)
* `docker_no_proxy_host`: optional hostname to be excluded from proxying (needed eg. for runners)
* `install_docker_compose`: defaults to false - true when you want to install docker-compose
* `docker_compose_version`: defaults to 1.15.0 - docker-compose version

Please note both `http_proxy_address` and `https_proxy_address` are also used by other roles,
like `ansible-gitlab`.

Authors
------------------

Wojciech Kaczmarek
