### Docker swarm
* Deploy docker swarm to manage galera and ghost docker images

### Docker
* Install docker package and a few dependencies

### Galera stack
* Deploy galera with master
* Check health of galera based on # of nodes
* Scale down galera master and scale up node
* expose port 3306 on the host VM

### Ghost
* Deploy ghost and expose port 80 on the host VM

### Keealived
* Deploy keepalived to handle VRRP IPs

### Networking
* Set up network on network interfaces on all nodes