#!/usr/bin/env bash

export OS_AUTH_URL=https://keystone.ic-debamf1.in.pan-net.eu:5000/v2.0

# change it if you need.
export OS_TENANT_NAME="alien_for_iaas"

unset OS_PROJECT_ID
unset OS_PROJECT_NAME
unset OS_USER_DOMAIN_NAME

export OS_USERNAME="svetlana.duryninapan-net.eu"

export OS_REGION_NAME="RegionOne"
# Don't leave a blank variable, unset it if it was empty
if [ -z "$OS_REGION_NAME" ]; then unset OS_REGION_NAME; fi

export OS_ENDPOINT_TYPE=publicURL
export OS_INTERFACE=public
export OS_IDENTITY_API_VERSION=2
export OS_CACERT="$CI_PROJECT_DIR/envs/$TARGET_ENVIRONMENT/rootcax1.crt"