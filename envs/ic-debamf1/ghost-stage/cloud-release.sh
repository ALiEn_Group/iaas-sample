#!/usr/bin/env bash

# Setup Pan-Net Cloud release.
# Availible values are "li" for Lithium and "be" for Berylium.
export TF_VAR_cloud_release="be"

# Echo cloud release in the pipeline
echo "Using the Cloud release - $TF_VAR_cloud_release"
