# envs
 
Environment specific configuration needed to deploy application with proper configuration into the right tenant. It's split into datacenters and tenants in each datacenter.
*  cloud-release.sh - ALiEn choose the right configuration procedure based on cloud release variable. Will handle differences between Lithium and Berilium in terraform files
*  *.cert - SSL certificate to access OpenStack API in datacenter
*  openrc.sh - environment variables for the OpenStack tenant 
*  vars.tfvars - terraform variables to specific tenant
